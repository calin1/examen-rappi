package com.carlossardi.rappireddit.components;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.carlossardi.rappireddit.R;
import com.carlossardi.rappireddit.common.Tools;
import com.carlossardi.rappireddit.io.AppController;
import com.carlossardi.rappireddit.models.Children;
import com.carlossardi.rappireddit.models.Data;

import java.util.List;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class ListViewRappiAdapter extends ArrayAdapter<Data> {

    private final Activity context;
    private final List<Data> itemname;
    private final Children imgid;
    ImageLoader imageLoader;
    Tools tool;


    public ListViewRappiAdapter(Activity context,List<Data> itemname, Children imgid) {
        super(context, R.layout.list_item, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
        this.imgid=imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        tool = new Tools();
        imageLoader = AppController.getInstance().getImageLoader();
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_item, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.headerTxt);
        TextView txtSbus = (TextView) rowView.findViewById(R.id.subscribersTxt);
        TextView extratxt = (TextView) rowView.findViewById(R.id.bannerTxt);
        TextView txtDate = (TextView) rowView.findViewById(R.id.dateTxt);
        ImageView thumbLocal = (ImageView) rowView.findViewById(R.id.thumbnailLocal);
        NetworkImageView thumbNail = (NetworkImageView) rowView
                .findViewById(R.id.thumbnail);
        NetworkImageView  banner  = (NetworkImageView) rowView.findViewById(R.id.banner);

        txtTitle.setText(itemname.get(position).getData().getDisplay_name());
        txtSbus.setText(String.valueOf(itemname.get(position).getData().getSubscribers()));
        extratxt.setText(imgid.getChildren().get(position).getData().getPublic_description());
        txtDate.setText(tool.getDateCurrentTimeZone(itemname.get(position).getData().getCreated_utc()));
        thumbLocal.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.thumbnail_local));
        // thumbnail image
        if(!imgid.getChildren().get(position).getData().getIcon_img().isEmpty()){
            thumbNail.setImageUrl(imgid.getChildren().get(position).getData().getIcon_img(), imageLoader);
        }else{
            thumbNail.setVisibility(View.GONE);
            thumbLocal.setVisibility(View.VISIBLE);
        }
        if(!itemname.get(position).getData().getBanner_img().isEmpty()){
            banner.setImageUrl(itemname.get(position).getData().getBanner_img(),imageLoader);
        }


        return rowView;

    }


}

