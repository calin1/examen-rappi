package com.carlossardi.rappireddit.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.carlossardi.rappireddit.models.Children;

import java.io.Serializable;

import static com.carlossardi.rappireddit.common.Constants.SHARED_PREFERENCES;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class Session implements Serializable {

    private String ch;
    Context ctx;
    private static transient Session theInstance = null;
    private static transient final Object lock = new Object();

    public String loadSession(Context ctx){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return prefs.getString(SHARED_PREFERENCES,"");
    }

    public void setSession(Context ctx,String response){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SHARED_PREFERENCES, response);
        editor.commit();
    }



}
