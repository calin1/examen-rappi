package com.carlossardi.rappireddit.common;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class Constants {


    // SharedPreferences
    public static final String SHARED_PREFERENCES = "SharedPrefs";
    public static final String NO_POSEE_SESION = "No posee en sesión la información";
    public static final String ITEM_SELECCIONADO = "item";
    public static final String BUSCANDO = "Buscando en sesión";
    public static final String ACCION_NO_PERMITIDA = "Acción no permitida";
    public static final String BUSCAND_SERVIDOR = "Buscando en servidor";
}
