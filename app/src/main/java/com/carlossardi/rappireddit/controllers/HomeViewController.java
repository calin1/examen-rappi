package com.carlossardi.rappireddit.controllers;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import com.carlossardi.rappireddit.R;
import com.carlossardi.rappireddit.common.Constants;
import com.carlossardi.rappireddit.common.Session;
import com.carlossardi.rappireddit.common.Tools;
import com.carlossardi.rappireddit.io.HttpServer;


public class HomeViewController extends Activity {



    private CoordinatorLayout coordinatorLayout;
    Tools tool;
    HttpServer hs;
    Session se;
    int a;
    ImageView iconoSad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        a = 0;
    }

    private void init() {

        tool = new Tools();
        hs = new HttpServer(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_home);
        iconoSad = (ImageView) findViewById(R.id.iconoSad);

    }

    @Override
    protected void onResume() {
        super.onResume();


            if(tool.internetAvailable(this)){
                //TODO si hay internet
                if(a==0){
                    hs.execute();
                    a++;
                }
            }else {
                //TODO no hay internet
                final ProgressDialog dialog = ProgressDialog.show(this, "", Constants.BUSCANDO,
                        true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        se =  new Session();
                        String json =  se.loadSession(HomeViewController.this);
                        if(!json.isEmpty()){
                            //TODO si posee el json en sesion
                            Intent i = new Intent(HomeViewController.this,ListViewController.class);
                            startActivity(i);
                        }else{
                            //TODO no posee nada en sesion
                            iconoSad.setVisibility(View.VISIBLE);
                            dialog.dismiss();
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, Constants.NO_POSEE_SESION, Snackbar.LENGTH_LONG);
                            snackbar.show();

                        }
                    }
                }, 4000);

            }



    }


}
