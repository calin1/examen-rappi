package com.carlossardi.rappireddit.controllers;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.carlossardi.rappireddit.R;
import com.carlossardi.rappireddit.common.Constants;
import com.carlossardi.rappireddit.common.Session;
import com.carlossardi.rappireddit.components.ListViewRappiAdapter;
import com.carlossardi.rappireddit.models.Children;
import com.carlossardi.rappireddit.models.Data;
import com.google.gson.Gson;

import java.util.List;

import static android.R.attr.data;

public class ListViewController extends Activity {

    Session se;
    ListView yourListView;
    List<Data> itemname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        se =  new Session();
        String json =  se.loadSession(this);
        Children gs = new Gson().fromJson(json,Children.class);
        //ListViewRappiAdapter customAdapter = new ListViewRappiAdapter(this,itemname);
        itemname = gs.getChildren();
        ListViewRappiAdapter adapter=new ListViewRappiAdapter(this, itemname, gs);
        adapter.notifyDataSetChanged();
        yourListView = (ListView) findViewById(R.id.subRedditsLv);
        yourListView.setAdapter(adapter);
        yourListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(ListViewController.this,DetalleViewController.class);
                i.putExtra(Constants.ITEM_SELECCIONADO,position);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
