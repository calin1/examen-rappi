package com.carlossardi.rappireddit.controllers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.carlossardi.rappireddit.R;
import com.carlossardi.rappireddit.common.Constants;
import com.carlossardi.rappireddit.common.Session;
import com.carlossardi.rappireddit.common.Tools;
import com.carlossardi.rappireddit.components.LruBitmapCache;
import com.carlossardi.rappireddit.io.AppController;
import com.carlossardi.rappireddit.models.Children;
import com.google.gson.Gson;

public class DetalleViewController extends AppCompatActivity {

    Session se;
    Tools tool;
    int itemMostrar;
    ImageLoader imageLoader;
    ImageView bannerLocal,iconoLocal;
    TextView tituloTxt,url,descripcion,over,subs,created;
    NetworkImageView icono,banner;
    private ProgressDialog dialog;
    CoordinatorLayout cl;
    ActionBar ab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        itemMostrar = i.getIntExtra(Constants.ITEM_SELECCIONADO,0);
        tool = new Tools();

        configViews(itemMostrar);

//        Toast.makeText(DetalleViewController.this,gs.getChildren().get(itemMostrar).getData().getDisplay_name(),
//                Toast.LENGTH_SHORT).show();
    }

    private void configViews(int itemMostrar) {
        dialog = new ProgressDialog(this);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        ab = getSupportActionBar();

        cl = (CoordinatorLayout) findViewById(R.id.activity_detalle_view_controller);
        imageLoader = AppController.getInstance().getImageLoader();
        se =  new Session();
        String json =  se.loadSession(this);
        Children gs = new Gson().fromJson(json,Children.class);
        if(!gs.getChildren().get(itemMostrar).getData().getKey_color().isEmpty()){
            ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor(gs.getChildren().get(itemMostrar).getData().getKey_color())));
        }
        setTitle(gs.getChildren().get(itemMostrar).getData().getDisplay_name());
        tituloTxt = (TextView) findViewById(R.id.tituloDetalle);
        icono = (NetworkImageView) findViewById(R.id.thumbnailDetalle);
        banner = (NetworkImageView) findViewById(R.id.bannerDetalle);
        url = (TextView) findViewById(R.id.urlDetalle);
        descripcion = (TextView) findViewById(R.id.descripcionDetalle);
        bannerLocal = (ImageView) findViewById(R.id.bannerDetalleLocal);
        created = (TextView) findViewById(R.id.createdDetalle);
        over = (TextView) findViewById(R.id.over18Detalle);
        subs = (TextView) findViewById(R.id.subsDetallle);
        iconoLocal = (ImageView) findViewById(R.id.thumbnailDetalleLocal);


        subs.setText("Subs: "+String.valueOf(gs.getChildren().get(itemMostrar).getData().getSubscribers()));
        over.setText("Over18: "+String.valueOf(gs.getChildren().get(itemMostrar).getData().isOver18()));
        created.setText("Created: " + tool.getDateCurrentTimeZone(gs.getChildren().get(itemMostrar).getData().getCreated_utc()));
        url.setText(gs.getChildren().get(itemMostrar).getData().getUrl());
        tituloTxt.setText(gs.getChildren().get(itemMostrar).getData().getHeader_title());
        if(!gs.getChildren().get(itemMostrar).getData().getBanner_img().isEmpty()){
            banner.setImageUrl(gs.getChildren().get(itemMostrar).getData().getBanner_img(),imageLoader);
        }else{
            bannerLocal.setVisibility(View.VISIBLE);
            banner.setVisibility(View.GONE);
        }
        if(!gs.getChildren().get(itemMostrar).getData().getIcon_img().isEmpty()){
            icono.setImageUrl(gs.getChildren().get(itemMostrar).getData().getIcon_img(),imageLoader);
        }else {
            icono.setVisibility(View.GONE);
            iconoLocal.setVisibility(View.VISIBLE);
        }

        descripcion.setText(gs.getChildren().get(itemMostrar).getData().getPublic_description());



    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    //and this to handle actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Snackbar snackbar = Snackbar
                    .make(cl, Constants.ACCION_NO_PERMITIDA, Snackbar.LENGTH_LONG);
            snackbar.show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
