package com.carlossardi.rappireddit.io;

import android.content.Context;

import com.carlossardi.rappireddit.common.Session;
import com.carlossardi.rappireddit.models.Children;
import com.carlossardi.rappireddit.models.SubReddits;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class ServerResult {

    public void storeSession(String response, Context ctx) throws JSONException {

        JSONObject objectJSON = new JSONObject(response);
        response = objectJSON.getString("data");
        Session se = new Session();
        se.setSession(ctx,response);

        //Children gs = new Gson().fromJson(response,Children.class);

        return;

    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
