package com.carlossardi.rappireddit.io;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.carlossardi.rappireddit.common.Constants;
import com.carlossardi.rappireddit.controllers.HomeViewController;
import com.carlossardi.rappireddit.controllers.ListViewController;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class HttpServer extends AsyncTask<Object, Object, StringBuilder> {

    Context ctx;
    private ProgressDialog dialog;

    public HttpServer(Context ctx) {
        dialog = new ProgressDialog(ctx);
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.dialog.setMessage(Constants.BUSCAND_SERVIDOR);
        this.dialog.show();

    }

    @Override
    protected StringBuilder doInBackground(Object... params) {

        HttpsURLConnection urlConnection = null;
        URL url2;
        String url = "https://www.reddit.com/reddits.json";
        int rc;
        StringBuilder result = new StringBuilder();

        try {
            url2 = new URL(url);
            urlConnection = (HttpsURLConnection) url2
                    .openConnection();
            rc = urlConnection.getResponseCode();
            InputStream in;

            if(rc >= 400)
                in = urlConnection.getErrorStream();
            else
                in = urlConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        urlConnection.disconnect();

        return result;
    }

    @Override
    protected void onPostExecute(StringBuilder aVoid) {
        super.onPostExecute(aVoid);
        ServerResult sr = new ServerResult();
        if(sr.isJSONValid(aVoid.toString())){
            try {
                sr.storeSession(aVoid.toString(),ctx);
                Intent intent = new Intent(ctx, ListViewController.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.getApplicationContext().startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
