package com.carlossardi.rappireddit.models;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class Data {

    private String kind;

    private SubReddits data;

    public SubReddits getData() {
        return data;
    }

    public void setData(SubReddits data) {
        this.data = data;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }


}
