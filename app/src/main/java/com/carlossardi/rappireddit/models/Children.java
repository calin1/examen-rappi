package com.carlossardi.rappireddit.models;

import java.util.List;

/**
 * Created by CarlosSardi on 1/9/17.
 */

public class Children {

    private List<Data> children;

    public List<Data> getChildren() {
        return children;
    }

    public void setChildren(List<Data> children) {
        this.children = children;
    }

}
